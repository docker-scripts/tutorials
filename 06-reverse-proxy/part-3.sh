#!/bin/bash -i

TITLE="Reverse Proxy"
DESCRIPTION="Advanced topics"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. Resolving the IP of the container
: ##################################################

#
# Let's look again at the configuration of a domain:
#
##

:; cd /var/ds/revproxy/

:; cat domains/site1.example.com.conf nginx

##
#
# The 'proxy_pass' directive seems to forward the request back to
# revproxy itself, thus creating an infinite loop.
#
##

#
# However this is not the case, because the domain 'site1.example.com',
# inside the docker virtual network, resolves to the IP of the
# container that serves this domain. We can verify this with 'ping':
#
##

:; ds exec apt install -y iputils-ping

:; ds shell ping site1.example.com -c2

:; ping site1.example.com -c2

#
# Inside the container and outside the container, 'site1.example.com'
# is resolved to different IPs. How does it happen? The script that
# creates the container has also these lines of code:
#
##

:; sed -n -e '/local network/,+6p' /opt/docker-scripts/ds/src/cmd/create.sh

##
#
# So, for each domain that is served by the container, we append a
# '--network-alias' option to the command docker create. This is
# something like the '/etc/hosts' for the docker virtual network where
# the container is connected, and will instruct the domain resolver of
# this virtual network to resolve this alias (domain name) to the IP
# of the container.
#
##

: ##################################################
: ##  2. Set the real IP
: ##################################################

#
# A container that is behind a reverse proxy gets the HTTP request
# from the proxy, not from the client. So, the client IP that it sees
# is that of the reverse proxy. You can check this on the access logs
# of nginx or apache2 inside the container.
#
##

:; ds shell ping revproxy -c2

#
# Notice the IP of the revproxy container, on the ping output above.
#
##

:; cd /var/ds/test/site1.example.com/

:; ds shell tail /var/log/nginx/access.log

#
# Notice that on the logs there is the IP of revproxy.
#
##

:; cd /var/ds/test/site2.example.com/

:; ds exec tail /var/log/apache2/access.log

#
# The 'revproxy' is configured to send the IP of the client to the
# container, using the header 'X-Forwarded-For'. However the nginx or
# apache2 (or any other webserver) inside the container must be
# configured to use this header for getting the real IP of the
# client. Webservers don’t do this by default for security reasons
# (HTTP headers can be spoofed easily). That’s why you also have to
# tell the webserver the IP (or network) of the 'revproxy', so that it
# can trust the header 'X-Forwarded-For' that comes from it.
#
##

#
# We can enable this configuration with the script
# 'inject/set_real_ip.sh':
#
##

:;:; dir=/opt/docker-scripts/revproxy
:; cat $dir/test/app1/inject/set_real_ip.sh

:; cd /var/ds/test/site1.example.com/

:; ds inject set_real_ip.sh

:; ds restart
:;:; sleep 2

:; curl -k https://site1.example.com/

:; ds exec tail /var/log/nginx/access.log

#
# Notice that now there is a different IP that is being recorded on
# the logs.
#
##

#
# For apache2:
#
##

:; cat $dir/test/app2/inject/set_real_ip.sh

:; cd /var/ds/test/site2.example.com/

:; ds inject set_real_ip.sh

:; curl -k https://site2.example.com/

:; ds exec tail /var/log/apache2/access.log
