#!/bin/bash -i

TITLE="Reverse Proxy"
DESCRIPTION="

Testing revproxy with simple web apps.

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. Setup domains
: ##################################################

#
# For a simple test we are going to use the domains
# 'site1.example.com', 'alias1.example.com' and 'site2.example.com',
# which are fake domains. So, let’s add them first to '/etc/hosts', so
# that we are able to resolve them:
#
##

:;:; sed -i /etc/hosts -e '/example\.com/d'

:; /usr/bin/cat <<'EOF' >> /etc/hosts
127.0.0.1    site1.example.com
127.0.0.1    alias1.example.com
127.0.0.1    alias2.example.com
127.0.0.1    site2.example.com
EOF

:; grep example.com /etc/hosts

:; ping site1.example.com -c3

##

: ##################################################
: ## 2. The test app
: ##################################################

#
# We are going to use the scripts on 'revproxy/test/app1' in order to
# install a couple of simple apps.
#
##

:;:; dir=/opt/docker-scripts/revproxy
:; tree $dir/test/app1/

:; cat $dir/test/app1/Dockerfile

#
# The 'Dockerfile' just installs 'nginx' and 'ssl-cert' (which
# provides snakeoil fake ssl certs).
#
##

:; cat $dir/test/app1/inject/setup.sh

#
# The configuration script 'inject/setup.sh' just enables HTTPS and
# displays the DOMAIN on the front page.
#
##

:; cat $dir/test/app1/settings.sh

#
# The variable DOMAIN contains the main domain of the application, and
# the (optional) variable DOMAINS may contain other names (or aliases)
# for this application.
#
##

: ##################################################
: ## 3. Install simple containers
: ##################################################

#
# Lets’s install a couple of containers that are based on the test app
# and use the test domains.
#
##

:; ds init revproxy/test/app1 @test/site1.example.com

:; cd /var/ds/test/site1.example.com/

:; cat settings.sh

:; sed -i settings.sh -e '/^DOMAIN=/ c DOMAIN=site1.example.com'

:; sed -i settings.sh -e '/^#DOMAINS=/ c DOMAINS="alias1.example.com alias2.example.com"'

:; sed -i settings.sh -e '/^CONTAINER=/ c CONTAINER=revproxy-test-site1'

:; cat settings.sh

#
# IMPORTANT: Modifying CONTAINER is optional, but DOMAIN and DOMAINS
# should look as shown above (using *example.com* instead of
# *example.org*).
#
##

#
# Notice that we are not forwarding the ports 80/443 from the host to
# the container.
#
##

:; ds make

:; docker ps

:; docker image ls

#
# Let’s install another container for the domain 'site2.example.com'
#
##

:; ds init revproxy/test/app2 @test/site2.example.com

:; cd /var/ds/test/site2.example.com/

:; cat settings.sh

:; sed -i settings.sh -e '/^DOMAIN=/ c DOMAIN=site2.example.com'

:; sed -i settings.sh -e '/^#DOMAINS=/d'

:; sed -i settings.sh -e '/^CONTAINER=/ c CONTAINER=revproxy-test-site2'

:; cat settings.sh

#
# There are no aliases in this case, so the variable DOMAINS is not
# needed.
#
##

:; ds make

:; docker ps

:; docker image ls

: ##################################################
: ## 4. Test the reverse proxy
: ##################################################

#
# We can use 'curl' to test that we can access both sites through the
# reverse proxy.
#
##

:; curl -k https://site1.example.com

:; curl -k https://alias1.example.com

:; curl -k https://alias2.example.com

:; curl -k https://site2.example.com

#
# The option '-k, --insecure' is needed because we are using fake
# domains for testing, and we can’t get a certificate for them.
# 
##

: ##################################################
: ## 5. Check the configuration of revproxy
: ##################################################

:; ds @revproxy domains-ls

:; cd /var/ds/revproxy/

:; ls domains/

:; cat domains/site1.example.com.conf nginx

:; cat domains/site2.example.com.conf nginx

##
#
# We didn’t do anything to setup the configuration of revproxy for
# these domains, how did it happen? The key is the variable DOMAIN on
# 'settings.sh'. When we run 'ds make', it calls in turn 'ds create',
# and the function that creates a container has also these lines of
# code:
#
##

:; sed -n -e '/add DOMAIN to revproxy/,+4p' /opt/docker-scripts/ds/src/cmd/create.sh

##

: ##################################################
: ## 6. The command: 'ds revproxy'
: ##################################################

#
# The command 'ds revproxy' is a global one, which is installed by the
# container revproxy. Being "global" means that it can be used by any
# container. It helps these containers to interact with the revproxy
# container, for example to register a domain, to get an SSL
# certificate for this domain, etc.
#
##

:; ds revproxy

##
#
# It is usually located at '~/.ds/cmd/revproxy.sh':
#
##

:; cat ~/.ds/cmd/revproxy.sh

##
#
# Let’s use it to mange the domain of our test containers:
#
##

:; cd /var/ds/test/site1.example.com/

#
# List the domain:
#
##

:; ds revproxy ls

:; ds @revproxy domains-ls

:; ds revproxy type

:; ds revproxy path

:; cat /var/ds/revproxy/domains/site1.example.com.conf nginx

##
#
# Remove the configuration of the domain from revproxy:
#
##

:; ds revproxy rm

:; ds revproxy ls

:; ds @revproxy domains-ls

:; ls /var/ds/revproxy/domains/

#
# Add it back:
#
##

:; ds revproxy add

:; ds revproxy ls

:; ds @revproxy domains-ls

:; ls /var/ds/revproxy/domains/

#
# The commands 'ds revproxy' and 'ds @revproxy ...' may seem like
# similar, however the first one is a command that runs in the context
# (directory) of the container ('test/site1.example.com/'), while the
# second one is a command that runs on the context (directory) of the
# 'revproxy'. So, the first command has access to the variable DOMAIN
# of the container, and the second one does not.
#
##
