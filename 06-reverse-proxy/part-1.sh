#!/bin/bash -i

TITLE="Reverse Proxy"
DESCRIPTION="

Installing and testing revproxy.

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. What is a Reverse Proxy
: ##################################################

#
# A reverse proxy stands between the clients (browsers) and the sites
# that are served by different containers. All client requests (on
# ports 80 and 443) are forwarded by the host to the reverse proxy
# container. Based on the domain of the request, the proxy container
# makes another HTTP request to the appropriate container (that serves
# the given domain), and forwards the response to the original
# client. So, it acts like a man-in-the-middle between the clients and
# the containers that serve the different domains. It can also be
# thought like an HTTP gateway or hub for different domains.
#
##

#
# Besides making it possible to serve several domains from different
# containers, the reverse proxy container can also help with other
# tasks, like:
#
#  * getting and maintaining a letsencrypt certificate for each of the
#    domains that it is managing
#
#  * redirecting HTTP requests to HTTPS automatically
#
#  * improving performance by caching HTTP responses
#
#  * shielding (or protecting) the domains from some attacks from the
#    internet
#
##

#
# If there was no reverse proxy, each container that serves a domain
# would have to take care of these tasks by itself. For example each
# container would have to get and maintain letsencrypt certificates
# for the domains that it serves. However the reverse proxy makes
# simple the life of the containers by taking care of these tasks for
# each of them.
#
##

: ##################################################
: ##  2. Install revproxy
: ##################################################

:; ds pull revproxy

:; ds init revproxy @revproxy

:; cd /var/ds/revproxy/

:; cat settings.sh

#
# Modify SSL_CERT_EMAIL
#
##

:; sed -i settings.sh -e 's/user@example.org/user@gmail.com/'

:; cat settings.sh

:; ds make

:; ls

: ##################################################
: ##  3. Check config files
: ##################################################

:; tree

##
#
# The directories 'domains/' and 'letsencrypt/' are empty because no
# domains are being managed yet.
#
##

:; cat conf.d/default.conf nginx

##
#
# The default configuration redirects all HTTP requests to HTTPS,
# except for the requests that are needed by certbot and letsencrypt
# to verify that we own the domain for which we are requesting a
# certificate.
#
##

:; cat example.conf nginx

##
#
# The config file 'example.conf' is used like a template for the
# configuration of the domains that are managed. Of course,
# 'example.org' is replaced by the domain that is managed, and the SSL
# certificates are replaced by the path of the letsencrypt
# certificates for the domain. It should work well for most of the
# cases, but you can customize it, if needed, and the modifications
# will propagate to the new domains that are created.
#
##

: ##################################################
: ##  4. Test some commands
: ##################################################

#
# Let's add some domains with 'ds domains-add':
#
##

:; ds domains-add {site1,alias1,alias2}.example.com

:; ds domains-add site2.example.com

:; ds domains-ls

:; ls domains/

:; cat domains/site1.example.com.conf nginx

:; cat domains/site2.example.com.conf nginx

##
#
# We can try to get a letsencrypt certificate for these domains, but
# it is going to fail, for obvious reasons:
#
##

:; ds get-ssl-cert {site1,alias1,alias2}.example.com

:; ds get-ssl-cert site2.example.com

:; ds get-ssl-cert site1.test.com

##
#
# Let’s remove them:
#
##

:; ds domains-rm site1.example.com site2.example.com

:; ds domains-ls

:; ls domains/

:; ds del-ssl-cert {site1,site2}.example.com site1.test.com
