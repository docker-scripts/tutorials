#!/bin/bash -x

# remove the test containers
ds @test/site1.example.com remove
ds @test/site2.example.com remove

# remove the directories of the test containers
rm -rf /var/ds/test/

# remove revproxy
ds @revproxy remove
rm -rf /var/ds/revproxy

# remove test domains fom /etc/hosts
sed -i /etc/hosts -e '/example\.com/d'
