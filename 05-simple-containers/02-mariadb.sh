#!/bin/bash -i

TITLE="Simple Containers -- MariaDB"
DESCRIPTION="

This container may be used by applications that need a MySQL/MariaDB database.

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. The scripts
: ##################################################

#
# The scripts are located at https://gitlab.com/docker-scripts/mariadb
# We can get them with 'ds pull':
#
##

:; ds pull mariadb

:; tree /opt/docker-scripts/mariadb/

:; cd /opt/docker-scripts/mariadb/

:; cat Dockerfile docker

:; cat settings.sh

##
#
# Usually the MariaDB container needs to be accessed from other
# containers in the same virtual (docker) LAN. In this case we don’t
# need to uncomment the PORTS setting. However, if we want it to be
# accessible from outside, for example from the internet, we should
# uncomment it.
#
##

:; cat cmd/create.sh

##
#
# It mounts outside the container the configuration
# ('/etc/mysql/mariadb.conf.d'), data ('/var/lib/mysql'), logs,
# etc. So, when we rebuild the container, the configuration and data
# will persist (will not be lost).
#
##

:; cat cmd/config.sh

:; cat inject/mariadb.sh

: ##################################################
: ##  2. Create a container
: ##################################################

:; ds init mariadb @db1

:; cd /var/ds/db1/

:; ds make

:; ls

:; ls data/

:; ds shell systemctl status mariadb

:; ds shell mariadb -e 'show databases'

: ##################################################
: ##  3. Use it from another container
: ##################################################

#
# Let’s create a test container that needs a DB.
#
##

:; ds pull ubuntu

:; ds init ubuntu @test1

:; cd /var/ds/test1/

:; cat settings.sh

:; sed -i settings.sh -e '/PORTS/d'

:; cat settings.sh

#
# We removed PORTS because we don’t need to access it from the
# internet.
#
##

:; ds make

#
# To create and manage a database for this container we can use the
# command 'ds mariadb':
#
##

:; ds mariadb

#
# It complains that DBHOST is missing from 'settings.sh', but actually
# we also need to define as well DBNAME, DBUSER and DBPASS:
#
##

:; /usr/bin/cat <<'EOF' >> settings.sh

DBHOST='db1'
DBNAME='test1'
DBUSER='test1'
DBPASS='pass1'
EOF

:; cat settings.sh

##
#
# Here, *db1* is the name of the mariadb container that we built
# previously. The rest of the variables can be anything, but to be
# organized and systematic, we are setting the DBNAME and DBUSER to
# the name of the container ('test1' in this case).
#
##

:; ds mariadb

:; ds mariadb create

:; ds mariadb sql "show databases"

:; ds @db1 exec mariadb -e 'show databases'

:; ls /var/ds/db1/data/

#
# Drop the database 'test1':
#
##

:; ds mariadb drop

:; ds mariadb sql "show databases"

#
# To access the database from inside the container we need to install
# a MariaDB client first.
#
##

:; ds exec apt install --yes mariadb-client

:; ds mariadb create

:; ds shell mariadb --host=db1 --user=test1 --password=pass1 -e 'show databases'

#
# The name of the mariadb container ('db1') can be used as the host,
# because Docker provides an internal DNS service that resolves the
# name of the container to the IP of the container.
#
##

:; mkdir -p inject

:; /usr/bin/cat <<'EOF' > inject/db_list.sh
#!/bin/bash -x

source /host/settings.sh
mariadb="mariadb --host=$DBHOST --user=$DBUSER --password=$DBPASS"

$mariadb -e 'show databases'
EOF

:; cat inject/db_list.sh

:; chmod +x inject/db_list.sh

:; ds inject db_list.sh

: ##################################################
: ##  4. The global command: 'ds mariadb'
: ##################################################

:; ds mariadb

##
#
# It is called global because it can be called in the context
# (directory) of any other container (not in the directory of the
# mariadb container, as usual). It can be thought of as an extension
# of the basic functionality of the docker-scripts framework. It is
# not installed by default when DS is installed because it makes sense
# and is useful only when a mariadb container is installed.
#
##

#
# It is installed when the mariadb container is created, by placing
# the script 'cmd/mariadb.sh' in the directory '/root/.ds/cmd/' (where
# '/root/.ds/' is the default value of DSDIR).
#
##

:; cat /opt/docker-scripts/mariadb/cmd/create.sh

#
# This global command assumes (requires) that the container that calls
# it has defined (on 'settings.sh') the variables: DBHOST, DBNAME,
# DBUSER, DBPASS. The variable DBHOST keeps the name of the MariaDB
# container.
#
##

:; cat ~/.ds/cmd/mariadb.sh

##

: ##################################################
: ##  5. Clean up
: ##################################################

#
# After you are done with testing, you can clean up like this:
#

: ds @test1 remove
: ds @db1 remove
: rm -rf /var/ds/{test1,db1}
: rm -rf /opt/docker-scripts/{ubuntu,mariadb}
: rm ~/.ds/cmd/mariadb.sh

