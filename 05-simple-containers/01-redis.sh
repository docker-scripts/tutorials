#!/bin/bash -i

TITLE="Simple Containers -- Redis"
DESCRIPTION="

This is a helper (auxiliary) container, that may be used by other
applications to improve caching efficiency.

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. The scripts
: ##################################################

#
# The scripts are located at https://gitlab.com/docker-scripts/redis
# We can get them with 'ds pull':
#
##

:; ds pull redis

:; tree /opt/docker-scripts/redis/

:; cd /opt/docker-scripts/redis/

:; cat Dockerfile

:; cat settings.sh

##
#
# The comments explain the purpose and usage of the settings. Usually
# this container is used in a virtual Docker LAN, in order to serve as
# a cache server for other containers on the same LAN. In this case we
# don’t need to forward any ports and the setting PORTS should be
# commented. However, if we want to access it from outside (for
# example from internet), we should uncomment PORTS, which enables
# port forwarding for 6379. In this case we should also make sure to
# uncomment the PASS setting and to set a strong password to it.
#
##

:; cat cmd/create.sh

##
#
# The default create command is extended by creating the directories
# 'conf', 'data' and 'logs' on the host and mounting to them the
# directories '/etc/redis', '/var/lib/redis' and '/var/log/redis' from
# the container.
#
##

:; cat cmd/config.sh

:; cat inject/redis.sh

##
#
# We do 'source /host/settings.sh' because we need to get the variable
# PASS, which we use below. If it is defined, then we change the
# configuration file to require a password. Otherwise, redis can be
# accessed without a password.
#
##

: ##################################################
: ##  2. Make a container
: ##################################################

:; ds init redis @redis

:; cd /var/ds/redis/

:;:; password=abcd1234

:; sed -i settings.sh -e "/^PASS=/ c PASS='$password'"

:; cat settings.sh

:; ds make

:; tree

:; tail logs/redis-server.log

: ##################################################
: ##  3. Test it
: ##################################################

:; ds exec redis-cli PING

:; ds exec redis-cli -a "$password" PING

:; ds exec redis-cli --no-auth-warning -a "$password" PING

:; ds exec redis-cli --no-auth-warning -a "$password" FLUSHALL

:; ds exec redis-cli --no-auth-warning -a "$password" SET key1 test1

:; ds exec redis-cli --no-auth-warning -a "$password" GET key1

: ##################################################
: ##  4. Clean up
: ##################################################

:; ds remove

:; cd -

:; rm -rf /var/ds/redis/

:; rm -rf /opt/docker-scripts/redis/
