#!/bin/bash -i

TITLE="Simple Containers -- PostgreSQL"
DESCRIPTION="

This container may be used by applications that need a PostgreSQL database.

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. The scripts
: ##################################################

#
# The scripts are located at https://gitlab.com/docker-scripts/postgresql
# We can get them with 'ds pull':
#
##

:; ds pull postgresql

:; tree /opt/docker-scripts/postgresql/

:; cd /opt/docker-scripts/postgresql/

:; cat Dockerfile docker

:; cat settings.sh

##
#
# Usually the PostgreSQL container needs to be accessed from other
# containers in the same virtual (docker) LAN. In this case we don’t
# need to uncomment the PORTS setting. However, if we want it to be
# accessible from outside, for example from the internet, we should
# uncomment it.
#
##

:; cat cmd/create.sh

##
#
# It mounts outside the container the configuration
# ('/etc/postgresql'), data ('/var/lib/postgresql'), logs, etc. So,
# when we rebuild the container, the configuration and data will
# persist (will not be lost).
#
##

:; cat cmd/config.sh

:; cat inject/postgresql.sh

: ##################################################
: ##  2. Create a container
: ##################################################

:; ds init postgresql @db2

:; cd /var/ds/db2/

:; ds make

:; ds shell ps ax

:; ds shell systemctl status postgresql

:; ds pg

#:; ds pg psql --list

: ##################################################
: ##  3. Use it from another container
: ##################################################

#
# Let’s create a test container that needs a DB.
#
##

:; ds pull ubuntu

:; ds init ubuntu @test2

:; cd /var/ds/test2/

:; cat settings.sh

:; sed -i settings.sh -e '/PORTS/d'

:; cat settings.sh

#
# We removed PORTS because we don’t need to access it from the
# internet.
#
##

:; ds make

#
# To create and manage a database for this container we can use the
# command 'ds postgresql':
#
##

:; ds postgresql

#
# It complains that DB_HOST is missing from 'settings.sh', but
# actually we also need to define as well DB_NAME, DB_USER and
# DB_PASS:
#
##

:; /usr/bin/cat <<'EOF' >> settings.sh

DB_HOST='db2'
DB_NAME='test2'
DB_USER='test2'
DB_PASS='pass2'
EOF

:; cat settings.sh

#
# Here, *db2* is the name of the postgresql container that we built
# previously. The rest of the variables can be anything, but to be
# organized and systematic, we are setting the DB_NAME and DB_USER to
# the name of the container ('test2' in this case).
#
##

:; ds postgresql

:; ds postgresql create

:; ds postgresql sql "SELECT datname FROM pg_database"

#:; ds @db2 pg psql --list

#
# To access the database from inside the container we need to install
# a PostgreSQL client first.
#
##

:; ds exec apt install --yes postgresql-client

:; ds shell PGPASSWORD=pass2 psql --host=db2 --username=test2  -c "SELECT datname FROM pg_database"

#
# The name of the postgresql container ('db2') can be used as the
# host, because Docker provides an internal DNS service that resolves
# the name of the container to the IP of the container.
#
##

:; mkdir -p inject

:; /usr/bin/cat <<'EOF' > inject/db_list.sh
#!/bin/bash -x

source /host/settings.sh

export PGPASSWORD=$DB_PASS
psql="psql --host=$DB_HOST --username=$DB_USER"

$psql -c "SELECT datname FROM pg_database"
EOF

:; cat inject/db_list.sh

:; chmod +x inject/db_list.sh

:; ds inject db_list.sh

: ##################################################
: ##  4. The global command: 'ds postgresql'
: ##################################################

:; ds postgresql

#
# It is called global because it can be called in the context
# (directory) of any other container (not in the directory of the
# mariadb container, as usual). It can be thought of as an extension
# of the basic functionality of the docker-scripts framework. It is
# not installed by default when DS is installed because it makes sense
# and is useful only when a postgresql container is installed.
#
##

#
# It is installed when the postgresql container is created, by placing
# the script 'cmd/postgresql.sh' in the directory '/root/.ds/cmd/'
# (where '/root/.ds/' is the default value of DSDIR).
#
##

:; cat /opt/docker-scripts/postgresql/cmd/create.sh

#
# This global command assumes (requires) that the container that calls
# it has defined (on 'settings.sh') the variables: DB_HOST, DB_NAME,
# DB_USER, DB_PASS. The variable DB_HOST keeps the name of the
# PostgreSQL container.
#
##

:; cat ~/.ds/cmd/postgresql.sh

##

: ##################################################
: ##  5. Clean up
: ##################################################

#
# After you are done with testing, you can clean up like this:
#

: ds @test2 remove
: ds @db2 remove
: rm -rf /var/ds/{test2,db2}
: rm -rf /opt/docker-scripts/{ubuntu,postgresql}
: rm ~/.ds/cmd/postgresql.sh
