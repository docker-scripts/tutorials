#!/bin/bash -i

TITLE="More examples"
DESCRIPTION="

Usually the scripts of the containers are kept in git repositories,
not in a local directory. Let’s try some of the simple scripts that
are located at https://gitlab.com/docker-scripts

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. Build a simple ubuntu container
: ##################################################

#
# Get the scripts:
#
##

:;:; cd

:; ds pull ubuntu

#
# As you can see from the output, 'ds pull' is basically a 'git clone'
# of 'https://gitlab.com/docker-scripts/ubuntu' to the directory
# '/opt/docker-scripts/'.
#
##

#
# The command 'ds' (without arguments) shows some information about
# the general configuration and setup of docker-scripts:
#
##

:; ds

##
#
# The environment variable DSDIR tells 'ds' where to look for
# configurations. It loads '$DSDIR/config.sh', which contains these
# settings:
#
#  REPO — the base URL for the script repos
#  SCRIPTS — the default directory where the scripts are saved
#  CONTAINERS — the default directory where the containers are created
#
# These can be customized if needed, but usually the defaults are OK.
#
##

#
# Let’s have a look at 'ubuntu' scripts that we just downloaded:
#
##

:; cd /opt/docker-scripts/ubuntu/

:; tree

:; cat Dockerfile

:; cat settings.sh

#
# Port 2201 of the host is forwarded to port 22 of the container, in
# order to allow SSH access to the container.
#
##

:; cat cmd/create.sh

#
# Although 'cmd_create' is extended, it actually does not do any extra
# things.
#
##

:; cat cmd/config.sh

#
# 'setup.sh' is the script below:
#
##

:; cat inject/setup.sh

##
#
# It generates an SSH key pair, if one is not already present, and
# corrects the SSH config.
#
##

: ##################################################
: ##  2. Initialize a directory for the container
: ##################################################

:;:; cd

:; ds init ubuntu @server1

#
# Note that because the directory of the container ('@server1') does
# not start with './' or '../' or '/', it is interpreted as a
# subdirectory of '$CONTAINERS', which is set on '~/.ds/config.sh' and
# has a default value of '/var/ds/'.
#
##

:; cd /var/ds/server1/

:; ls

:; cat settings.sh

#
# Initialization just copies 'settings.sh' on the directory of the
# container and sets some values for IMAGE and CONTAINER. The default
# value for IMAGE is the name of the scripts' directory, and the
# default value for CONTAINER is the name of the container’s
# directory. You may change them if you wish, but the default values
# are usually OK.
#
##

: ##################################################
: ##  3. Build and test the container
: ##################################################

:; ds make

:; ls

:; cat sshkey

##
#
# The file 'sshkey' has been created by 'inject/setup.sh'. We can use
# it to ssh to the container, like this:
#
##

:;:; alias ssh='ssh -o StrictHostKeyChecking=no'
:; ssh -p 2201 -i sshkey root@localhost ps ax

:; ssh -p 2201 -i sshkey root@localhost cat /etc/os-release

#
# After this tutorial is finished, try to login like this:
#

: ssh -p 2201 -i /var/ds/server1/sshkey root@localhost

##
