#!/bin/bash -i

TITLE="More examples"
DESCRIPTION="

Usually the scripts of the containers are kept in git repositories,
not in a local directory. Let’s try some of the simple scripts that
are located at https://gitlab.com/docker-scripts

"

source $(dirname $0)/../playlib

: ##################################################
: ##  4. Build another container
: ##################################################

#
# We don’t need to get the scripts with 'ds pull ubuntu', because we
# did it for the first container. However it doesn’t hurt to update
# them:
#
##

:; ds pull ubuntu

#
# Initialize a directory for the container:
#
##

:; ds init ubuntu @server2

:; cd /var/ds/server2/

:; ls

:; cat settings.sh

:; ds make

#
# The container cannot start because the port 2201 is being used by
# the other container.
#
##

:; docker ps --format '{{.Names}}: {{.Ports}}'

#
# Let’s edit 'settings.sh', change the port to 2202, and try again:
#
##

:; sed -i settings.sh -e 's/2201/2202/'

:; cat settings.sh

:; ds make

:; docker ps

:; docker ps --format '{{.Names}}: {{.Ports}}'

:; ls

:; cat sshkey

##

:;:; alias ssh='ssh -o StrictHostKeyChecking=no'
:; ssh -p 2202 -i sshkey root@localhost hostname

:; ssh -p 2202 -i sshkey root@localhost cat /etc/os-release

: ##################################################
: ##  5. Build a debian container
: ##################################################

#
# The scripts for a simple debian container are almost identical to
# those for a simple ubuntu container, except that the Dockerfile has
# the line 'include(bullseye)' instead of 'include(jammy)'.
#
##

:; ds pull debian

:; ds init debian @server3

:; ls /var/ds/

:; cd /var/ds/server3/

:; cat settings.sh

:; sed -i settings.sh -e 's/2201/2203/'

:; cat settings.sh

:; ds make

:; docker ps

:; docker ps --format '{{.Names}}: {{.Ports}}'

:; ls

:; cat sshkey

:; ssh -p 2203 -i sshkey root@localhost cat /etc/os-release

: ##################################################
: ##  6. Use a branch of the scripts
: ##################################################

#
# The command 'ds pull' can also have a branch argument:
#
##

:; ds pull

#
# Let’s use the branch 'buster' of the scripts' repo this time:
#
##

:; ds pull debian buster

:; ls /opt/docker-scripts/

:; ds init debian-buster @server4

:; cd /var/ds/server4/

:; sed -i settings.sh -e 's/2201/2204/'

:; cat settings.sh

:; ds make

:; ds exec cat /etc/os-release

:; ssh -p 2204 -i sshkey root@localhost cat /etc/os-release

: ##################################################
: ##  7. Get the scripts with git clone
: ##################################################

#
# This time let’s get the scripts with 'git clone' instead of 'ds pull'.
#
##

:; repo=https://gitlab.com/docker-scripts/debian
:; dir=/opt/docker-scripts/test/buster
:; git clone -b buster $repo $dir

:; ds init test/buster @server5

:; cd /var/ds/server5/

:; cat settings.sh

:; sed -i settings.sh -e 's/2201/2205/'

:; cat settings.sh

:; ds make

:; ds exec hostname

:; ds exec cat /etc/os-release

:; ssh -p 2205 -i sshkey root@localhost hostname

