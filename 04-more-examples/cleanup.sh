#!/bin/bash -x

# remove containers
for dir in /var/ds/server[1-5] ; do
    cd $dir
    ds remove
done

# remove the directories of the containers
rm -rf /var/ds/server[1-5]

# remove the scripts
rm -rf /opt/docker-scripts/{ubuntu,debian,debian-buster,test}

# check
docker ps -a
docker image ls
ls /var/ds/
ls /opt/docker-scripts/
