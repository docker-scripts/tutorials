# Interactive tutorials about docker-scripts

> **NOTE:**
>
> These interactive tutorials are based on the examples at the docs of
> docker-scripts: https://docker-scripts.gitlab.io/
>
> It is better to follow those examples manually. However, if you are
> a bit lazy, you can try these interactive tutorials instead.

## **Tip:** Run these tutorials in a virtual machine

The commands of `docker-scripts` are meant to be used in a server (or
VPS) and they need to be executed as *root*. These interactive
tutorials need to be executed as *root* too. When trying them, it is
safer to use a dedicated VPS or a virtual machine. It is not
recommended to try them directly on your personal machine
(laptop). They are not meant to do harm, but you never know (maybe
there is any bug or something else).

There are lots of ways for running a virtual machine, but in the docs
there is a section that explains [how to create and setup a LXD
container](https://docker-scripts.gitlab.io/appendices/create-test-container-with-lxd.html),
which is quite easy and convenient.

## Running the tutorials

When you run a tutorial like this: `./01-intro.sh`, it actually
re-executes itself like this: `./play ./01-intro.sh`. This is done by
`playlib.sh`, which is included at the top of each tutorial.

The script `./play` depends on `highlight` and `expect`, so you
need to install these dependencies as well:

``` bash
apt install highlight expect
```

In order to run a tutorial scripts without `./play`, call it like
this: `./01-intro.sh noplay`

An alternative could also be `./01-intro.sh autoplay` or
`./01-intro.sh auto` (they are the same). In this case you don't have
to press **[Enter]** on each step because it will continue
automatically after a short delay.

## How to reset the progress of the tutorials

Usually tutorials build on top of each other. For example each
tutorial on `07-sniproxy/` expects the previous ones to be completed
successfully and in order.

Let's say that after completing `07-sniproxy/04-proxy-protocol.sh` you
play around a little bit, and now you want to continue with
`07-sniproxy/05-chaining-proxies.sh.sh`. To make sure that the state
of the tutorials is as expected, you can first clean up everything,
and then replay quickly the previous tutorials, like this:

``` bash
07-sniproxy/cleanup.sh
for tutorial in 07-sniproxy/0[1-4]* ; do $tutorial noplay ; done
```