#!/bin/bash -i

# Check that the container has already been built
if [[ ! -d /var/test/app1/logs/ ]]; then
    echo 2>&1 "Please run this first: 03-example1/03-build-container.sh"
    exit 1
fi

TITLE="A basic example: 4. Play around"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

In this part we will try some more ds commands.

"

source $(dirname $0)/../playlib

: ##################################################
: # 4.1. Check the logs
: ##################################################

#
# The output of the 'ds' commands, besides being displayed on the
# screen, is also saved on a log file, for later inspection.
#
##

:;:; cd /var/test/app1/

:; tree

:; tail logs/app1-*.out -n 15

##

: ##################################################
: # 4.2 Get a shell inside the container
: ##################################################

#
# We can get a shell inside the container with the command 'ds shell'
#
##

#
# Unfortunately, this does not work well with these interactive
# tutorials.
#
##

: ##################################################
: # 4.3 Change the prompt of the container
: ##################################################

#
# As an example, we can run the script 'change-prompt.sh' inside
# the container. This is done with 'ds inject', which copies
# the script inside the container and executes it from there:
# 'ds inject change-prompt.sh'
#
##

#
# We can check the new prompt with 'ds shell', but it doesn't work
# well with these interactive scripts. However, lets have a look at
# the content of the script 'change-prompt.sh'.
#
##

:; cat /opt/docker-scripts/ds/src/inject/change-prompt.sh

##

#
# Try on your own these commands:
#

: cd /var/test/app1/
: ds shell
: ls -al
: exit
: ds inject change-prompt.sh
: ds shell
: ls -al
: exit
: cd -
