#!/bin/bash -i

# Check that the container directory has already been initialized.
if [[ ! -f /var/test/app1/settings.sh ]]; then
    echo 2>&1 "Please run this first: 03-example1/02-init-container-dir.sh"
    exit 1
fi

TITLE="A basic example: 3. Build the container"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

The third part is about building the container.

"

source $(dirname $0)/../playlib

:;:; cd /var/test/

#
# The command 'ds make' will build the image of the container, create
# the container, and start it. But first we need to be located in the
# directory of the container.
#
##

:; cd app1/

:; ds make

#
# Check the status of the container:
#
##

:; ds info

#
# Let’s also use the docker commands to check the status of the images
# and containers:
#
##

:; docker ps

:; docker image ls

#
# The command 'ds make', as well as all the other 'ds' commands, gets
# the name of the image and the container from 'settings.sh' on the
# current directory (by including it as a bash script). That’s why we
# need to be on the container directory before using any 'ds' command.
#
##
