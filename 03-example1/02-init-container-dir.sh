#!/bin/bash -i

# Check that the scripts have already been created.
if [[ ! -f /var/test/scripts1/Dockerfile ]]; then
    echo 2>&1 "Please run this first: 03-example1/01-create-scripts.sh"
    exit 1
fi

TITLE="A basic example: 2. Create a container dir"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

The second part is about initializing a directory for the container.

"

source $(dirname $0)/../playlib

:;:; cd /var/test/

:; ds init scripts1 @./app1

#
# The command 'ds init' creates a directory for the container, copies
# 'settings.sh' to it, and modifies APP, IMAGE and CONTAINER.
#
##

:; tree app1

:; cat app1/settings.sh

#
# The setting APP tells where the scripts for this container are
# located, IMAGE is the name of the docker image that will be built
# for this container (set by default to the name of the scripts
# directory), and CONTAINER is the name of the container (set by
# default to the name of the container's directory).
#
##

#
# You can edit 'settings.sh' and modify IMAGE and CONTAINER, if you
# wish, but you should not change the value of APP, otherwise the
# framework will not be able to find the correct Dockerfile and any
# other scripts related to this container.
#
##
