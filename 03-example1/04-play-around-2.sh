#!/bin/bash -i

# Check that the container has already been built
if [[ ! -d /var/test/app1/logs/ ]]; then
    echo 2>&1 "Please run this first: 03-example1/03-build-container.sh"
    exit 1
fi

TITLE="A basic example: 4. Play around"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

In this part we will try some more ds commands.

"

source $(dirname $0)/../playlib

: ##################################################
: # 4.4. Run commands inside the container
: ##################################################

#
# We can also execute commands inside the container with 'ds exec' and `ds shell`
#
##

:;:; cd /var/test/app1/

:; ds exec pwd

:; ds shell ls -lR

:; ds exec cat settings.sh

:; ds exec touch test.txt

:; ls -l

:; rm test.txt

#
# You will notice that the directory of the container (/var/test/app1)
# is mounted inside the container at '/host'. This helps sharing of
# data between the container and the host, for example in the case of
# backup/restore.
#
##

#
# Create a new user inside the container.
#
##

:; ds shell useradd -m user1

:; ds shell ls /home/

##

: ##################################################
: # 4.5. Use the debug options
: ##################################################

#
# If you wonder what Docker commands 'ds' is using behind the scenes,
# you can use the option "-d" to enable the setting SHOW_DOCKER_COMMANDS
#
##

:; ds -d stop

:; ds -d start

:; ds -d restart

:; ds -d exec ls /

#
# Or you can enable it using env variables.
#
##

:; SHOW_DOCKER_COMMANDS=true ds restart

:;:; export SHOW_DOCKER_COMMANDS=true
:; ds restart

:;:; unset SHOW_DOCKER_COMMANDS
:; ds restart

#
# Use the option "-x" to see all the steps that 'ds' takes while
# executing a command.
#
##

:; ds -x make

:; ds -x -d make

#
# Debug messages are being displayed on stderr.
#
##

:; ds -x make >/dev/null

#
# The options "-x" and "-d" should come right after 'ds' and before
# anything else. When they are used together, "-x" should come before
# "-d". This is not really flexible but that’s how it is.
#
##
