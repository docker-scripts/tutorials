#!/bin/bash -i

# Check that the app1 has already been built
if [[ ! -d /var/test/app1/logs/ ]]; then
    echo 2>&1 "Please run this first: 03-example1/03-build-container.sh"
    exit 1
fi

TITLE="A basic example: 5. Build a second container"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

In this part we will build a second container that uses the same
scripts as the first one.

"

source $(dirname $0)/../playlib

#
# The test directory so far looks like this:
#
##

:; tree /var/test/

##

: ##################################################
: # 5.1. Init the directory of the second container
: ##################################################

#
# To build another container based on 'scripts1' you should first
# initialize a directory for it:
#
##

:;:; cd /var/test/app1/

:; ds init ../scripts1 @../app2

:; tree /var/test/

#
# To understand the strange syntax of the command 'ds init' have a
# look at its help:
#
##

:; ds init

##
#
# Normally, the scripts are placed under the directory
# '/opt/docker-scripts/' and the containers under the directory
# '/var/ds/'. Relative or absolute directories are used rarely, for
# example for testing.
#
##

: ##################################################
: # 5.2. The settings of the second container
: ##################################################

:; cat /var/test/app2/settings.sh

#
# The name of the directory has been set as the name of the CONTAINER,
# but you can change it as long as it is unique among all the docker
# containers.
#
##

#
# The name of the IMAGE can also be changed, but usually the name of
# the scripts directory is a good setting.
#
##

#
# IMPORTANT: You should not change the value of APP, unless you move
# the scripts directory to another location.
#
##

: ##################################################
: # 5.3. Build the second container
: ##################################################

##

:; ds @../app2 info

:; ds @/var/test/app2 make

#
# If the *first* argument of 'ds' starts with '@', it switches to the
# specified container directory before interpreting the rest of the
# command. However it is usually more convenient to 'cd' to the
# container’s directory, before giving any 'ds' commands related to
# this container.
#
##

:; cd ../app2

:; ds info

##

: ##################################################
: # 5.4. Fix the configuration
: ##################################################

#
# If you check, you will notice that there is no user 'user1' inside
# the container:
##

:; ds shell ls /home/

#
# We need to do the same thing that we did for the first container,
# customize the command 'cmd_config()', so that a new user is added
# automatically when the container is constructed:
#
##

:; mkdir -p cmd

:; cp ../app1/cmd/config.sh cmd/

:; cat cmd/config.sh

:; ds make

:; ds shell ls /home/

#
# Notice however that we are making the same 'ds config' customization
# for both containers. If we need to make the same customization for
# all the containers of this type (containers that are based on
# 'scripts1'), it is better to make this customization to the code of
# the scripts:
#
##

:; cd /var/test/scripts1/

:; mkdir -p cmd

:; cp ../app1/cmd/config.sh cmd/

:; tree

:; cat cmd/config.sh

#
# Let's remove 'cmd/config.sh' from both containers 'app1' and 'app2':
#
##

:; rm -rf ../app1/cmd/

:; rm -rf ../app2/cmd/

:; tree ../app1/

:; tree ../app2/

#
# Let's check that 'user1' is still created when we rebuild the
# containers:
#
##

:; ds @../app1 make

:; ds @../app1 exec ls /home/

:; ds @../app2 make

:; ds @../app2 exec ls /home/
