#!/bin/bash -i

TITLE="A basic example: 6. Add a new ds command"
DESCRIPTION='

In this example we explore some basic concepts and commands of
docker-scripts.

We have already seen how to override the existing command "ds config".
In this part we will create a new "ds" command, which is done in
almost the same way.

'

source $(dirname $0)/../playlib

: ##################################################
: ##  6.1. Create "ds hello"
: ##################################################

#
# We want to create the command 'ds hello' that just prints a greeting
# from the container. We should create it on the scripts directory, so
# that it can be used by all the containers.
#
##

:; cd /var/test/scripts1/

:; mkdir -p cmd

:; /usr/bin/cat <<'EOF' > cmd/hello.sh
cmd_hello() {
    local name=$1
    [[ -n $name ]] || fail "Usage: hello <name>"

    echo "Hello $name from container '$CONTAINER'!"
}
EOF

:; tree .

:; cat cmd/hello.sh

##
#
# By convention, we create a bash file that is named after the
# command, in the subdirectory 'cmd/', and inside this file we declare a
# function that is named after the command and has the prefix
# 'cmd_'. In this example it is 'cmd_hello()'.
#
##

#
# The file 'cmd/hello.sh' doesn’t need to be executable because it is
# sourced (included) by 'ds'.
#
##

#
# Let’s try the command 'ds hello':
#
##

:; ds @../app1 hello

:; ds @../app1 hello Foo

:; cd /var/test/app2/

:; ds hello Foo

##
#
# Notice that the variables defined in the file 'settings.sh' of the
# container are available as global variables on the function of the
# command. In this example we are using the variable '$CONTAINER':
#
##

:; cat /var/test/scripts1/cmd/hello.sh

##
#
# Notice also that the arguments that are given after 'ds hello' are
# passed to the function 'cmd_hello()'. Actually, 'cmd_hello()'
# requires an argument and fails if this argument is missing.
#
##

#
# The function 'fail' is an auxiliary function defined by the
# framework:
#
##

:; head /opt/docker-scripts/ds/src/auxiliary.sh -n 7

##

: ##################################################
: ##  6.2. Define help
: ##################################################

#
# If you try the command 'ds help' you will also see 'hello' listed by
# the end of the output:

:; ds help

#
# To add a description to its help entry we can define the function
# 'cmd_hello_help()' on '/var/test/scripts1/cmd/hello.sh':
#
##

:; /usr/bin/cat <<'EOF' > /var/test/scripts1/cmd/hello.sh
cmd_hello_help() {
    cat <<_EOF
    hello <name>
        Print a greeting for <name> from the container.

_EOF
}

cmd_hello() {
    local name=$1
    [[ -n $name ]] || fail "Usage:\n$(cmd_hello_help)"

    echo "Hello $name from container '$CONTAINER'!"
}
EOF

:; cat /var/test/scripts1/cmd/hello.sh

:; ds help

##
#
# By convention, we append '_help' to the name of the cmd function
# (cmd_hello). In this example the help function is using 'cat' to
# output the text of the help, but you can also use 'echo' or
# something else.
#
##

: ##################################################
: ##  6.3. Customize a command
: ##################################################

#
# Let’s say that we want to customize the command 'ds hello' for the
# container 'app2', so that besides the greeting it also displays some
# info about the container.
#
##

#
# Let’s create the file 'cmd/hello.sh' inside '/var/test/app2/':
#
##

:; cd /var/test/app2/

:; mkdir -p cmd

:; /usr/bin/cat <<'EOF' > cmd/hello.sh
rename_function cmd_hello orig_cmd_hello
cmd_hello() {
    orig_cmd_hello "$@"
    ds info
}
EOF

:; cat cmd/hello.sh

##
#
# We are overriding the function 'cmd_hello()' by defining it again
# for the container. However, before doing that, we are saving the
# original function (defined by the scripts) by renaming it to
# 'orig_cmd_hello', so that we can use it inside the new
# function.
#
# Inside the new function we are calling the original one and we are
# passing all the arguments to it (if any) with "$@". We are also
# calling 'ds info' to display some information about the container
# and its state.
#
##

#
# To change the name of an existing function we are using the
# auxiliary function 'rename_function' which is defined by the
# framework.
#
##

:; head /opt/docker-scripts/ds/src/auxiliary.sh -n 15

#
# This is a dirty trick that we are using because Bash lacks proper
# OOP features. But as long as it gets the job done, who cares!
#
##

#
# Let's try the customized 'ds hello':
#
##

:; ds hello

:; ds hello Foo

#
# For the other container the output should be unchanged:
#
##

:; cd ../app1/

:; ds hello Foo
