#!/bin/bash -i

# Check that the container has already been built
if [[ ! -d /var/test/app1/logs/ ]]; then
    echo 2>&1 "Please run this first: 03-example1/03-build-container.sh"
    exit 1
fi

TITLE="A basic example: 4. Play around"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

In this part we will try some more ds commands.

"

source $(dirname $0)/../playlib

: ##################################################
: # 4.6. Customize the configuration
: ##################################################

#
# The command 'ds make' creates a new container from the image, and
# any customizations made to the old one are lost.
#
##

:; cd /var/test/app1/

:; ds make

#
# If we check, we will notice that we have lost the user that we
# created before inside the container:
#
##

:; ds shell ls /home/

#
# The command 'ds make' calls also 'ds config' after 'ds create':
#
##

:; ds -x make 2>&1 >/dev/null | tail

#
# We can customize 'ds config' like this:
#
##

:; mkdir -p cmd

:; /usr/bin/cat <<'EOF' > cmd/config.sh
cmd_config() {
    ds shell useradd -m user1
}
EOF

:; cat cmd/config.sh

#
# Let's check that when we rebuild the container, a new user will be
# added as well.
#
##

:; ds make

:; ds shell ls /home/

#
# The local file 'cmd/config.sh', which defines the function
# 'cmd_config()', is included by 'ds' and overrides the default
# 'cmd_config()' of the framework.
#
##

#
# The default 'config.sh' (defined by the framework) looks like this:
#
##

:; cat /opt/docker-scripts/ds/src/cmd/config.sh

##

#
# You can see that it does nothing, so it’s ok to override it with a
# local configuration script.
#
##
