#!/bin/bash -i

TITLE="A basic example: 1. Create the scripts"
DESCRIPTION="

In this example we explore some basic concepts and commands of
docker-scripts.

The first part is about creating some minimal scripts.

"

source $(dirname $0)/../playlib

#
# For this example let's work on the directory: /var/test/
#
##

:; mkdir -p /var/test

:; cd /var/test/

#
# Let’s create the scripts on the directory scripts1/
#
##

:; mkdir -p scripts1

:; echo 'include(bookworm)' > scripts1/Dockerfile

:; echo 'APP=scripts1'   > scripts1/settings.sh

:; tree

:; cat scripts1/Dockerfile

:; cat scripts1/settings.sh

#
# The Dockerfile defines the image of the container. It is like a
# normal Dockerfile, except that before being used to build the image,
# it is preprocessed by 'm4' in order to include other files, etc.
#
##

#
# The file 'bookworm' is provided by docker-scripts.
#
##

:; cat /opt/docker-scripts/ds/src/dockerfiles/bookworm docker

##
#
# There are more dockerfiles provided by docker-scripts, like: bionic,
# buster, bullseye, etc. Almost all of them start from an ubuntu or
# debian base image, install *systemd*, and make it the
# process/command that is started inside the container. Then *systemd*
# will take care of running any other services that might be installed
# inside the container, like cron, apache2, etc. This way the
# container can be used like a virtual machine.
#
##

:; tree /opt/docker-scripts/ds/src/dockerfiles/
