#!/bin/bash -i

TITLE="Installing docker-scripts"

source $(dirname $0)/playlib

#
# 1. Make sure that the dependencies are installed:
#
##

:; dependencies='git make m4 highlight tree'
:; apt install -y $dependencies

#
# 2. Get the code from GitLab:
#
##

:; repo='https://gitlab.com/docker-scripts/ds'
:; dir='/opt/docker-scripts/ds'
:; git clone $repo $dir

#
# 3. Install it:
#
##

:; cd '/opt/docker-scripts/ds/'

:; git pull

:; make install

#
# 4. Check that it is installed:
#
##

:; ds

:; ds -h
