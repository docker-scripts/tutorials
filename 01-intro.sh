#!/bin/bash -i

TITLE="What are docker-scripts"

export NODELIMITER=1
source $(dirname $0)/playlib

#
# docker-scripts are Bash scripts that are used to manage Docker
# containers.
#
##

#
# Docker commands have lots of arguments and options and they tend to
# be very long. It is tedious and error prone to type these commands
# manually, so recording them in Bash scripts is a necessity. However
# the scripts of different containers also have a lot of things in
# common, which would be nice to abstragate and reuse. And this is one
# of the things that docker-scripts does. It is like a framework that
# makes building and using such scripts easy.
#
##

#
# Next, run './02-install.sh' to install docker-scripts step-by-step.
#
##
