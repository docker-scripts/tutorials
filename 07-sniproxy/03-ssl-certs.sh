#!/bin/bash -i

TITLE="SNI Proxy"
DESCRIPTION="

Getting SSL certificates

"

source $(dirname $0)/../playlib

#
# With 'curl' we are using the option '-k, --insecure' because the
# domains that we are using for testing are fake ones, so we cannot
# get SSL certificates for them:
#
##

:; curl -k https://site1.example.com

:; curl -k https://alias1.example.com

:; curl -k https://site2.example.com

#
# But if you are using some real domains/subdomain that you own, you
# can get SSL certificates for them with the scripts
# 'inject/get_ssl_cert.sh':
#
##

:; dir=/opt/docker-scripts/sniproxy/test

:; cat $dir/app1/inject/get_ssl_cert.sh

:; cat $dir/app2/inject/get_ssl_cert.sh

#
# The first part (getting a certificate from letsencrypt) is the same
# for both scripts.  The differences are only on updating the
# configuration of nginx and apache2.
#
##

:; cd /var/ds/test/site1/

:; ds inject get_ssl_cert.sh

:; cd /var/ds/test/site2/

:; ds inject get_ssl_cert.sh
