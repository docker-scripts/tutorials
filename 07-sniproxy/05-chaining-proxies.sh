#!/bin/bash -i


TITLE="SNI Proxy"
DESCRIPTION="

Chaining proxies.

It is possible to place a revproxy behind a sniproxy, which might be a
useful setup in some cases.

For more details look at:
https://docker-scripts.gitlab.io/sniproxy.html#_chaining_proxies

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. Configure sniproxy
: ##################################################

#
# For the containers 'site3' and 'site4' we are going to use the
# domains 'site3.example.com' and 'site4.example.com'. Since these are
# fake domains, let’s add them first to '/etc/hosts':
#
##

:; /usr/bin/cat <<'EOF' >> /etc/hosts
127.0.0.1    site3.example.com
127.0.0.1    site4.example.com
EOF

:; grep example.com /etc/hosts

#
# Add these lines to the configuration of sniproxy:
#
##

:; cd /var/ds/sniproxy/

:; /usr/bin/cat <<'EOF' | sed -i etc/sniproxy.conf -e '/site2.example.com/ r /dev/stdin'

    # container: revproxy
    site3.example.com  revproxy  proxy_protocol
    site4.example.com  revproxy  proxy_protocol
EOF

:; cat etc/sniproxy.conf

:; ds restart

#
# We are using the container 'revproxy' as a backend for
# 'site3.example.com' and 'site4.example.com'. We are going to install
# this container right now.
#
##

: ##################################################
: ##  2. Install revproxy
: ##################################################

:; ds pull revproxy

:; ds init revproxy @revproxy

:; cd /var/ds/revproxy/

:; cat settings.sh

#
# Let's make sure to comment out PORTS, because the container should
# not publish the HTTP/HTTPS ports, since it is going to be behind
# sniproxy.
#
##

:; sed -i settings.sh -e 's/^PORTS=/#PORTS=/'

#
# Set an email address that is needed for requesting letsencrypt SSL
# certificates.
#
##

:; sed -i settings.sh -e '/SSL_CERT_EMAIL/ c SSL_CERT_EMAIL=user@gmail.com'

#
# Make sure to uncomment ENABLE_PROXY_PROTOCOL, since we have already
# enabled proxy_protocol on sniproxy.
#
##

:; sed -i settings.sh -e '/ENABLE_PROXY_PROTOCOL/ c ENABLE_PROXY_PROTOCOL=true'

:; cat settings.sh

:; ds make

: ##################################################
: ##  3. Install site3 and site4
: ##################################################

#
# Install site3:
#
##

:; ds init revproxy/test/app1 @test/site3

:; cd /var/ds/test/site3/

:; cat settings.sh

:; sed -i settings.sh -e '/DOMAIN=/ c DOMAIN=site3.example.com' -e '/DOMAINS=/d'

:; cat settings.sh

:; ds make

#
# Install site4:
#
##

:; ds init revproxy/test/app2 @test/site4

:; cd /var/ds/test/site4/

:; cat settings.sh

:; sed -i settings.sh -e '/DOMAIN=/ c DOMAIN=site4.example.com' -e '/DOMAINS=/d'

:; cat settings.sh

:; ds make

#
# Set real IP:
#
##

:; cd /var/ds/test/site3/

:; ds inject set_real_ip.sh

:; cd /var/ds/test/site4/

:; ds inject set_real_ip.sh

: ##################################################
: ##  4. Check
: ##################################################

#
# Try to access them:
#
##

:; curl -k https://site3.example.com

:; curl -k https://site4.example.com

#
# Check sniproxy logs:
#
##

:; cd /var/ds/sniproxy/

:; ds exec tail /var/log/sniproxy/access.log

#
# Check revproxy logs:
#
##

:; cd /var/ds/revproxy/

:; ds exec tail /var/log/nginx/site3.example.com-access.log

:; ds exec tail /var/log/nginx/site4.example.com-access.log

#
# Check site3 logs:
#
##

:; cd /var/ds/test/site3/

:; ds exec tail /var/log/nginx/access.log

#
# Check site4 logs:
#
##

:; cd /var/ds/test/site4/

:; ds exec tail /var/log/apache2/access.log

: ##################################################
: ##  5. Default backend
: ##################################################

#
# Let’s make revproxy the default backend:
#
##

:; cd /var/ds/sniproxy/

:; cat etc/sniproxy.conf

:; sed -i etc/sniproxy.conf -e '/site3.example.com/d'

:; sed -i etc/sniproxy.conf -e '/site4.example.com/d'

:; /usr/bin/cat <<'EOF' | sed -i etc/sniproxy.conf -e '/container: revproxy/ r /dev/stdin'
    .*  revproxy  proxy_protocol
EOF

:; sed -i etc/sniproxy.conf -e 's/container: revproxy/default container: revproxy/'

:; cat etc/sniproxy.conf

##
#
# The regular expression ".*" will match any domain name that has not
# been matched by the other rules.
#
##

:; ds restart
:;:; sleep 3

#
# Let's check that we can still access 'site3.example.com' and
# 'site4.example.com':
#
##

:; curl -k https://site3.example.com

:; curl -k https://site4.example.com
