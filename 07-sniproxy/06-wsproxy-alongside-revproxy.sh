#!/bin/bash -i

TITLE="SNI Proxy"
DESCRIPTION="

Installing wsproxy alongside revproxy.

It is possible to install wsproxy alongside revproxy, which might be a
useful setup in some cases. Both of them need to be placed behind a
sniproxy.

For more details look at:
https://docker-scripts.gitlab.io/sniproxy.html#_install_wsproxy_alongside_revproxy

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. Introduction
: ##################################################

#
# The container 'wsproxy' does the same job as 'revproxy', but is
# based on apache2 instead of nginx. Usually we don’t need to use both
# of them in parallel, except for some special cases. However, doing
# it as an exercise helps to understand better the inner workings of
# the framework.
#
##

#
# There are also some problems with using both of them at the same
# time:
#
# 1. Both of them publish the ports *80* and *443* to the host, so it
#    is not possible to start both of the containers at the same time.
#
# 2. Both of them define the global command 'ds revproxy', which is
#    stored at '$HOME/.ds/cmd/revproxy.sh'. When we install the second
#    container, it will overwrite the definition that was made by the
#    first one, and this will create some problems.
#
##

#
# We can solve the first problem by putting both of them behind
# 'sniproxy'. So, it is 'sniproxy' that publishes the HTTP/HTTPS
# ports, while 'revproxy' and 'wsproxy' don't publish them.
#
##

#
# We can workaround the second problem by explicitly defining the
# variable DSDIR, which by default is '$HOME/.ds'.
#
##

#
# The containers 'sniproxy', 'revproxy', 'site3' and 'site4' are
# already installed in the previous tutorial. In this tutorial we are
# going to install 'wsproxy' (behind 'sniproxy') and 'site5' and
# 'site6' (behind 'wsproxy').
#
##

: ##################################################
: ##  2. Configure sniproxy
: ##################################################

#
# For the containers 'site5' and 'site6' we are going to use the
# domains 'site5.example.com' and 'site6.example.com'. Since these are
# fake domains, let’s add them first to '/etc/hosts':
#
##

:; /usr/bin/cat <<'EOF' >> /etc/hosts
127.0.0.1    site5.example.com
127.0.0.1    site6.example.com
EOF

:; grep example.com /etc/hosts

#
# Let's also add entries for these domains on the configuration of
# 'sniproxy':
#
##

:; cd /var/ds/sniproxy/

:; /usr/bin/cat <<'EOF' | sed -i etc/sniproxy.conf -e '/container: revproxy/ e cat'
    # container: wsproxy
    site5.example.com  wsproxy  proxy_protocol
    site6.example.com  wsproxy  proxy_protocol

EOF

:; cat etc/sniproxy.conf

:; ds restart

: ##################################################
: ##  3. Install wsproxy
: ##################################################

:; ds pull wsproxy

:; ds init wsproxy @wsproxy

:; cd /var/ds/wsproxy/

:; cat settings.sh

:; sed -i settings.sh -e '/PORTS/,$d'

:; /usr/bin/cat <<'EOF' >> settings.sh
#PORTS="80:80 443:443"
SSL_CERT_EMAIL=test@gmail.com
ENABLE_PROXY_PROTOCOL=true
EOF

:; echo 'DSDIR=$HOME/.ds-wsproxy' >> settings.sh

:; cat settings.sh

#
# Adding the variable DSDIR, as shown above, is important.
#
##

:; ds make

#
# Let’s check that the command 'revproxy.sh' is saved in the path
# given on DSDIR:
#
##

:; tree ~/.ds-wsproxy/

##

: ##################################################
: ##  4. Install site5
: ##################################################

:; ds init revproxy/test/app1 @test/site5

:; cd /var/ds/test/site5/

:; cat settings.sh

:; sed -i settings.sh -e '/DOMAIN=/ c DOMAIN=site5.example.com' -e '/DOMAINS=/d'

:; echo 'DSDIR=$HOME/.ds-wsproxy' >> settings.sh

:; cat settings.sh

#
# Adding the variable DSDIR, as shown above, is important, because it
# allows 'site5' to use 'wsproxy' for managing the domain (instead of
# 'revproxy', which is the default).
#
##

:; ds make

:; ds inject set_real_ip.sh

: ##################################################
: ##  5. Install site6
: ##################################################

:; ds init revproxy/test/app2 @test/site6

:; cd /var/ds/test/site6/

:; cat settings.sh

:; sed -i settings.sh -e '/DOMAIN=/ c DOMAIN=site6.example.com' -e '/DOMAINS=/d'

:; echo 'DSDIR=$HOME/.ds-wsproxy' >> settings.sh

:; cat settings.sh

#
# Adding the variable DSDIR, as shown above, is important, because it
# allows 'site6' to use 'wsproxy' for managing the domain (instead of
# 'revproxy', which is the default).
#
##

:; ds make

:; ds inject set_real_ip.sh

: ##################################################
: ##  6. Check
: ##################################################

#
# Check 'revproxy' config files:
#
##

:; ds @revproxy domains-ls

:; ds @wsproxy domains-ls

:; ls /var/ds/revproxy/domains/

:; ls /var/ds/wsproxy/domains/

:; cd /var/ds/wsproxy/

:; cat domains/site5.example.com.conf

:; cat domains/site6.example.com.conf

#
# Try to access them:
#
##

:; curl -k https://site5.example.com

:; curl -k https://site6.example.com

#
# Check 'sniproxy' logs:
#
##

:; cd /var/ds/sniproxy/

:; ds exec tail /var/log/sniproxy/access.log

#
# Check 'wsproxy' logs:
#
##

:; cd /var/ds/wsproxy/

:; ds exec tail /var/log/apache2/site5.example.com-access.log

:; ds exec tail /var/log/apache2/site6.example.com-access.log

#
# Check 'site5' logs:
#
##

:; cd /var/ds/test/site5/

:; ds exec tail /var/log/nginx/access.log

#
# Check 'site6' logs:
#
##

:; cd /var/ds/test/site6/

:; ds exec tail /var/log/apache2/access.log
