#!/bin/bash -x

sed -i /etc/hosts -e '/example.com/d'

cd /var/ds/
for app in sniproxy revproxy wsproxy revproxy-dsnet1 test/site{1..8} ; do
    [[ -d /var/ds/$app ]] || continue
    ds @$app remove
    rm -rf /var/ds/$app
done

rm -rf ~/.ds-wsproxy ~/.ds-net1
docker network rm dsnet1

