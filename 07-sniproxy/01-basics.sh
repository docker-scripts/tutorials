#!/bin/bash -i

TITLE="SNI Proxy"
DESCRIPTION="

It uses the Server Name Indication (SNI) to figure out to which
backend server it should forward the HTTPS connections.

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. What is a SNI proxy
: ##################################################

#
# In some cases it is required that the HTTPS connection is
# established between the client and the backend server, and the SSL
# certificate is managed by the backend server, not by the proxy. The
# proxy just forwards the HTTPS requests and responses transparently
# (at the TCP level) between the client and the backend server,
# without even being able to peek at the traffic (because it is
# encrypted).
#
##

#
# Because the HTTPS connection between the client and the backend
# server is encrypted, and the proxy cannot peek into it, it cannot
# read the header 'Host' of the request. So, normally it does not know
# the domain to which the request is sent, and does not know where to
# forward the request.
#
##

#
# Here is where SNI comes to the rescue. It is an extension of the
# HTTPS protocol, which allows the client to specify the domain name
# of the request during the TLS/SSL handshake, before the HTTPS
# connection is established. This allows the proxy to forward the
# request to the correct backend.
#
##


: ##################################################
: ##  2. Install sniproxy
: ##################################################

#
# Get the scripts:
#
##

:; ds pull sniproxy

:; cd /opt/docker-scripts/sniproxy/

:; cat Dockerfile docker

:; cat settings.sh

:; cat cmd/create.sh

:; cat cmd/config.sh

:; cat inject/setup.sh

##
#
# Build the container:
#
##

:; ds init sniproxy @sniproxy

:; cd /var/ds/sniproxy/

:; cat settings.sh

:; ds make

:; tree

:; docker ps

##

: ##################################################
: ##  3. Install simple containers for testing
: ##################################################

#
# For a simple test we are going to use the domains
# 'site1.example.com', 'alias1.example.com' and 'site2.example.com',
# which are fake domains. So, let’s add them first to '/etc/hosts', so
# that we are able to resolve them:
#
##

:;:; sed -i /etc/hosts -e '/example\.com/d'
:; /usr/bin/cat <<'EOF' >> /etc/hosts
127.0.0.1    site1.example.com
127.0.0.1    alias1.example.com
127.0.0.1    site2.example.com
EOF

:; grep example.com /etc/hosts

:; ping site1.example.com -c2

#
# Let’s install a couple of containers that are based on the test app
# and use the test domains:
#
##

:; ds init sniproxy/test/app1 @test/site1

:; cd /var/ds/test/site1/

:; cat settings.sh

:; sed -i settings.sh -e '/^DOMAIN_NAMES=/ c DOMAIN_NAMES="site1.example.com alias1.example.com"'

:; cat settings.sh

:; ds make

:; docker ps

##
#
# Let’s install another container for the domain 'site2.example.com':
#
##

:; ds init sniproxy/test/app2 @test/site2

:; cd /var/ds/test/site2/

:; cat settings.sh

:; sed -i settings.sh -e '/^DOMAIN_NAMES=/ c DOMAIN_NAMES=site2.example.com'

:; cat settings.sh

:; ds make

:; docker ps

##

: ##################################################
: ##  4. Configure sniproxy
: ##################################################

#
# Let’s make a minimal configuration for sniproxy:
#
##

:; cd /var/ds/sniproxy/

:; cp etc/sniproxy.conf etc/sniproxy.conf.sample

:; /usr/bin/cat <<'EOF' > etc/sniproxy.conf
user daemon
pidfile /var/run/sniproxy.pid

listen 0.0.0.0:80 {
    proto http
}

listen 0.0.0.0:443 {
    proto tls
}

table {
    # container: site1
    site1.example.com  site1
    alias1.example.com site1

    # container: site2
    site2.example.com  site2
}
EOF

:; cat etc/sniproxy.conf

##
#
# Note that *site1* and *site2* are the names of the docker
# containers, but inside the docker network they can be resolved to
# the IPs of those containers. Since 'sniproxy' is also running in a
# container, on the same docker network, it is possible to reach these
# containers by their names. This is more convenient than using the
# IPs of those containers.
#
##

:; ds restart
:;:; sleep 3

#
# We need to restart the container, in order to load the new
# configuration.
#
##

: ##################################################
: ##  5. Acess the test sites
: ##################################################

#
# We can access them with 'curl':
#
##

:; curl -k https://site1.example.com

:; curl -k https://alias1.example.com

:; curl -k https://site2.example.com

:; cd /var/ds/test/site1/

:; ds exec tail /var/log/nginx/access.log

:; cd /var/ds/test/site2/

:; ds exec tail /var/log/apache2/access.log
