#!/bin/bash -i

TITLE="SNI Proxy"
DESCRIPTION="

Install containers in a different network.

For more details look at:
https://docker-scripts.gitlab.io/sniproxy.html#_install_containers_in_a_different_network

"

source $(dirname $0)/../playlib

: ##################################################
: ##  1. Introduction
: ##################################################

#
# The docker network that is used by docker-scripts is called
# 'dsnet'. All the containers are connected to it, by default. You can
# check it like this:
#
##

:; docker network ls

:; docker network inspect dsnet | grep Name

#
# This may be customized by changing NETWORK and SUBNET at
# '~/.ds/config.sh':
#
##

:; cat ~/.ds/config.sh

##
#
# However all the containers would still use the same network, by
# default.
#
##

#
# If we want to use a different network for some containers, we have
# to append these variables at 'settings.sh' of each container:
#
#     NETWORK='dsnet1'
#     SUBNET='172.28.0.0/16'
#
##

#
# These containers will be able to communicate with each-other, but
# not with the containers in the 'dsnet' network, including
# 'revproxy'.
#
##

#
# There are two solutions to this problem:
#
##

#
# 1. Connect 'revproxy' to the network 'dsnet1' as well (by default it
#    is connected only to 'dsnet').
#
##

#
# 2. Install another revproxy container, which is connected to
#    'dsnet1'. In this case we have two revproxy containers, so we
#    need to place a 'sniproxy' container in front of them.
#
#    Now, the problem is that 'sniproxy' by default is connected only
#    to the network 'dsnet'. We need to connect it to both networks,
#    so that it is able to reach both revproxy containers.
#
##

#
# Let's try both of these cases.
#
##

: ##################################################
: ##  2. Connect revproxy to both networks
: ##################################################

#
# By default 'revproxy' is connected to the network 'dsnet'. Let’s say
# that we want to connect it to the network 'dsnet1' as well.
#
##

#
# First, let's create the network 'dsnet1':
#
##

:; docker network ls

:; docker network create dsnet1 --subnet 172.20.0.0/16

:; docker network ls

:; docker network inspect dsnet1 | highlight -S json -O xterm256

##
#
# Now let's connect the container 'revproxy' to it:
#
##

:; docker network connect dsnet1 revproxy

:; docker network inspect dsnet1 | grep Name

#
# Let's check that 'revproxy' is actually connected to both of the
# networks.
#
##

:; cd /var/ds/revproxy/

:; ds exec apt install -y iproute2

:; ds exec ip addr

:; ds exec hostname -I

#
# We can also customize the command 'ds create', so that the container
# is automatically connected to the second network, whenever we
# recreate it:
#
##

:; mkdir -p cmd

:; /usr/bin/cat <<'EOF' > cmd/create.sh
rename_function cmd_create app_cmd_create
cmd_create() {
    app_cmd_create "$@"

    docker network create dsnet1 --subnet 172.20.0.0/16 2>/dev/null
    docker network connect dsnet1 $CONTAINER

    ds exec apt install -y iproute2
}
EOF

:; cat cmd/create.sh

#
# Now, whenever we run 'ds make', the customized cmd_create() will be
# called, which connects the container to the second network as well.
#
##

:; ds make

:; ds exec ip addr

:; ds exec hostname -I

#
# Let's remove the second network:
#
##

:; docker network disconnect --force dsnet1 revproxy

:; docker network rm dsnet1

:; docker network ls

:; rm cmd/create.sh

: ##################################################
: ##  3. Install a second revproxy
: ##################################################

#
# Now let’s explore the second case, where we install another revproxy
# container, which is connected to 'dsnet1', and 'sniproxy' is
# connected to both networks 'dsnet' and 'dsnet1'.
#
##

#
# For the second revproxy and all the containers associated with it,
# we need to use a different DSDIR (for the same reasons that we
# discussed when installing a 'revproxy' and 'wsproxy' in
# parallel). So, the variables that we need to append to 'settings.sh'
# of each of these containers are:
#
#     DSDIR=$HOME/.ds-net1
#     NETWORK='dsnet1'
#     SUBNET='172.28.0.0/16'
#
##

: ##################################################
: ##  3.1. Configure sniproxy
: ##################################################

#
# For the containers 'site7' and 'site8' we are going to use the
# domains 'site7.example.com' and 'site8.example.com'. Since these are
# fake domains, let’s add them first to '/etc/hosts':
#
##

:; /usr/bin/cat <<'EOF' >> /etc/hosts
127.0.0.1    site7.example.com
127.0.0.1    site8.example.com
EOF

:; grep example.com /etc/hosts

#
# Add these domains ('site7.example.com' and 'site8.example.com') to
# the configuration of 'sniproxy':
#
##

:; cd /var/ds/sniproxy/

:; cat etc/sniproxy.conf

:; /usr/bin/cat <<'EOF' | sed -i etc/sniproxy.conf -e '/container: revproxy/ e cat'
    # container: revproxy-dsnet1
    site7.example.com  revproxy-dsnet1  proxy_protocol
    site8.example.com  revproxy-dsnet1  proxy_protocol

EOF

:; cat etc/sniproxy.conf

##
#
# We are forwarding these domains to the container 'revproxy-dsnet1',
# which we are going to build later.
#
##

#
# Customize the 'cmd_create()' of 'sniproxy' to connect this container
# to the docker network 'dsnet1' (besides connecting to 'dsnet', which
# is done by default):
#
##

:; mkdir -p cmd

:; /usr/bin/cat <<'EOF' > cmd/create.sh
rename_function cmd_create app_cmd_create
cmd_create() {
    app_cmd_create "$@"

    docker network create dsnet1 --subnet 172.28.0.0/16 2>/dev/null
    docker network connect dsnet1 $CONTAINER

    ds exec apt install -y iproute2
}
EOF

:; cat cmd/create.sh

#
# Rebuild the container:
#
##

:; ds make

#
# Check that it is connected to both networks:
#
##

:; ds exec ip addr

:; ds exec hostname -I

: ##################################################
: ##  3.2. Install the container 'revproxy-dsnet1'
: ##################################################

:; ds init revproxy @revproxy-dsnet1

:; cd /var/ds/revproxy-dsnet1/

:; cat settings.sh

:; sed -i settings.sh -e '/PORTS/,$ d'

:; /usr/bin/cat <<'EOF' >> settings.sh
#PORTS="80:80 443:443"
SSL_CERT_EMAIL=test@gmail.com
ENABLE_PROXY_PROTOCOL=true

DSDIR=$HOME/.ds-net1
NETWORK='dsnet1'
SUBNET='172.28.0.0/16'
EOF

:; cat settings.sh

:; ds make

#
# Let’s check that the command 'revproxy.sh' is saved in the path
# given on DSDIR:
#
##

:; tree ~/.ds-net1/

#
# Let’s also check that we can access 'sniproxy' (but not 'revproxy',
# which is connected to another network):
#
##

:; ds exec apt install -y iputils-ping

:; ds exec ping sniproxy -c2

:; ds exec ping revproxy -c2

: ##################################################
: ##  3.3. Install 'site7' and 'site8'
: ##################################################

#
# Install 'site7':
#
##

:; ds init revproxy/test/app1 @test/site7

:; cd /var/ds/test/site7/

:; cat settings.sh

:; sed -i settings.sh -e '/DOMAIN=/ c DOMAIN=site7.example.com' -e '/DOMAINS=/d'

:; /usr/bin/cat <<'EOF' >> settings.sh

DSDIR=$HOME/.ds-net1
NETWORK='dsnet1'
SUBNET='172.28.0.0/16'
EOF

:; cat settings.sh

:; ds make

:; ds inject set_real_ip.sh

#
# Install 'site8':
#
##

:; ds init revproxy/test/app2 @test/site8

:; cd /var/ds/test/site8/

:; cat settings.sh

:; sed -i settings.sh -e '/DOMAIN=/ c DOMAIN=site8.example.com' -e '/DOMAINS=/d'

:; /usr/bin/cat <<'EOF' >> settings.sh

DSDIR=$HOME/.ds-net1
NETWORK='dsnet1'
SUBNET='172.28.0.0/16'
EOF

:; cat settings.sh

:; ds make

:; ds inject set_real_ip.sh

: ##################################################
: ##  3.4. Check
: ##################################################

#
# Check revproxy config files:
#
##

:; ds @revproxy domains-ls

:; ds @revproxy-dsnet1 domains-ls

:; ls /var/ds/revproxy/domains/

:; ls /var/ds/revproxy-dsnet1/domains/

:; cd /var/ds/revproxy-dsnet1/

:; cat domains/site7.example.com.conf nginx

:; cat domains/site8.example.com.conf nginx

#
# Try to access the domains:
#
##

:; curl -k https://site3.example.com

:; curl -k https://site4.example.com

:; curl -k https://site7.example.com

:; curl -k https://site8.example.com

#
# Check that container 'site3' can access 'site4', but not 'site7' or
# 'site8':
#
##

:; cd /var/ds/test/site3/

:; ds exec apt install -y iputils-ping

:; ds exec ping site4 -c3

:; ds exec ping site7 -c3

:; ds exec ping site8 -c3

#
# Check that container 'site7' can access 'site8', but not 'site3' or
# 'site4':
#
##

:; cd /var/ds/test/site7/

:; ds exec apt install -y iputils-ping

:; ds exec ping site8 -c3

:; ds exec ping site3 -c3

:; ds exec ping site4 -c3

:; ds exec ping 172.18.0.2 -c3
