#!/bin/bash -i

TITLE="SNI Proxy"
DESCRIPTION="

Customize sniproxy logs

"

source $(dirname $0)/../playlib

#
# By default, sniproxy errors and warnings are logged to syslog:
#
##

:; cd /var/ds/sniproxy/

:; ds exec tail /var/log/syslog

##
#
# We can customize logging on the configuration file:
#
##

:; cat etc/sniproxy.conf

:; /usr/bin/cat <<'EOF' | sed -i etc/sniproxy.conf -e '/table {/ e cat'
error_log {
    filename /var/log/sniproxy/error.log
    priority notice
}

access_log {
    filename /var/log/sniproxy/access.log
    priority notice
}

EOF

:; cat etc/sniproxy.conf

:; ds restart
:;:; sleep 2

#
# Let's check that this is working:
#
##

:; curl -k https://site1.example.com

:; curl -k https://alias1.example.com

:; curl -k https://site2.example.com

:; ds exec tail /var/log/sniproxy/access.log

:; ds exec tail /var/log/sniproxy/error.log
