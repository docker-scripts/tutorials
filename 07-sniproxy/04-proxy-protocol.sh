#!/bin/bash -i

TITLE="SNI Proxy"
DESCRIPTION="

Enable proxy-protocol

"

source $(dirname $0)/../playlib

#
# If we check the logs on 'site1' and 'site2' we will notice that the
# requests are logged as coming from the IP of 'sniproxy'.
#
##

:; ds @sniproxy exec hostname -I

:; cd /var/ds/test/site1/

:; ds exec tail /var/log/nginx/access.log

:; cd /var/ds/test/site2/

:; ds exec tail /var/log/apache2/access.log

##
#
# That’s correct because the TCP requests are forwarded from sniproxy
# to these containers.
#
##

#
# Is it possible to get and log instead the IP of the client that is
# making the request?
#
##

#
# In the case of a Reverse Proxy, it is possible to send the IP of the
# client in a header (for example 'X-Forwarded-For') and the container
# can get and use this IP as the real IP of the client. However, in
# the case of a SNI Proxy this is not possible, because the proxy does
# not make a new connection to the backend, it just forwards the
# coming connection to it. Because this connection is encrypted, the
# proxy is not able to peek to the headers or the content of the HTTPS
# traffic between the client and the server, and neither can modify
# them.
#
##

#
# For this reason, the *Proxy Protocol* was invented, as a kind of
# addition in front of the HTTPS protocol. It allows the proxy to pass
# to the backend server information about the real IP of the client,
# etc.
#
##

#
# It has to be enabled both on the proxy and on the backend
# servers. If it is activated only on the proxy, the backend server
# will get confused, because it is expecting plain HTTPS and it gets
# something else instead. And vice-versa.
#
##

#
# To enable the Proxy Protocol on sniproxy, add "proxy_protocol" on
# 'etc/sniproxy.conf', like this:
#
##

:; cd /var/ds/sniproxy/

:; sed -i etc/sniproxy.conf -e '/table {/ , /}/ d'

:; /usr/bin/cat <<'EOF' >> etc/sniproxy.conf
table {
    # container: site1
    site1.example.com  site1  proxy_protocol
    alias1.example.com site1  proxy_protocol

    # container: site2
    site2.example.com  site2  proxy_protocol
}
EOF

:; cat etc/sniproxy.conf

:; grep proxy_protocol etc/sniproxy.conf

:; ds restart

#
# On the containers, enable proxy_protocol by injecting the script
# 'enable_proxy_protocol.sh':
#
##

:; dir=/opt/docker-scripts/sniproxy/test

:; cat $dir/app1/inject/enable_proxy_protocol.sh

:; cat $dir/app2/inject/enable_proxy_protocol.sh

:; cd /var/ds/test/site1/

:; ds inject enable_proxy_protocol.sh

:; cd /var/ds/test/site2/

:; ds inject enable_proxy_protocol.sh

#
# Let’s check again the logs:
#
##

:; curl -k https://site1.example.com

:; curl -k https://site2.example.com

:; cd /var/ds/test/site1/

:; ds exec tail /var/log/nginx/access.log

:; cd /var/ds/test/site2/

:; ds exec tail /var/log/apache2/access.log
